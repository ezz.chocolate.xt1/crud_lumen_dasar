<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


    
    use Illuminate\Database\Console\Seeds\WithoutModelEvents;
    
    use App\Models\MUser;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
    use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run(): void
        {
           
            $tableName = (new MUser)->getTable();
            if (Schema::hasTable($tableName)) {
                $rowCount = MUser::count();
                if ($rowCount > 0) {
                    MUser::truncate();
                }
                
            }
            
            $scheme = [
            [
            'nama' => 'KASINO',
            'nik' => '12345',
            'telp' => '021123',
            'alamat' => 'PULOGADUNG NO 1 BLOK A',
            
            'created_by' => 'SEEDER',
            'created_at' => Carbon::now(),
            'updated_at' => null,
            'updated_by' => null,
            'deleted_by' => null,
            'deleted_at' => null,
            ],
            
            ];
            
            MUser::insert($scheme);
        }
}
