<?php

namespace App\Http\Controllers;

use Illuminate\Http\{Request, JsonResponse};
use Illuminate\Support\Facades\URL;
use App\Models\MStore;
use App\Models\PublicModel;
use App\Models\MKeyApi;

class MStoreController extends Controller
{
	protected $judul_halaman_notif;
	
	public function __construct()
	{
		$this->judul_halaman_notif = 'Form Master Toko';
	}
	
	public function paging(Request $request): JsonResponse
	{
		$URL =  URL::current();

		if (!isset($request->search)) {
			$count = (new MStore())->count();
			$arr_pagination = (new PublicModel())->pagination_without_search($URL, $request->limit, $request->offset);
			$todos =(new MStore())->get_data_($request->search, $arr_pagination);
		} else {
			$arr_pagination = (new PublicModel())->pagination_without_search($URL, $request->limit, $request->offset, $request->search);
			$todos =  (new MStore())->get_data_($request->search, $arr_pagination);
			$count = $todos->count();
		}

		return response()->json(
			(new PublicModel())->array_respon_200_table($todos, $count, $arr_pagination),
			200
		);
	}
	
	
	public function destroy(Request $request, int $id): JsonResponse
	{
		//echo "<pre>";echo print_r($request);die();
		$user_id = $request->userid;

		try {
			$todo = MStore::findOrFail($id);
			MStore::where('id', $id)->update(['deleted_by' => $user_id]);
			$todo->delete();
			//untuk me-restore softdelete
			//$this->MUom->where('id', $id)->withTrashed()->restore();

			//return successful response
			return response()->json([
				'status' => true,
				'message' => $this->judul_halaman_notif . ' deleted successfully.',
				'user' => $todo
			], 201);
		} catch (\Exception $e) {

			//return error message
			return response()->json([
				'status' => false,
				'message' => $this->judul_halaman_notif . ' failed delete.',
			], 409);
		}
	}
	
	public function store(Request $request): JsonResponse
	{
		//echo "<pre>";echo print_r($request);die();
		$user_id = $request->userid;
		
		$data = $this->validate($request, [
			'data.storeCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/|unique:m_store,storeCode',
			'data.bpCustCode' => 'regex:/^[a-zA-Z0-9\s]+$/',
			'data.address' => 'regex:/^[a-zA-Z0-9\s]+$/',
			'data.phone' => 'regex:/^[a-zA-Z0-9\s]+$/',
			'data.storeName' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.priceCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.discCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.whsCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.level' => '',
		]);

		try {

			$data['data']['created_by'] = $user_id;

			//create table insert dan return id tabel
			$todos = MStore::create($data['data']);

			//return successful response
			return response()->json([
				'status' => true,
				'message' => $this->judul_halaman_notif . ' created successfully.',
				//'data' => $data['data']
			], 201);
		} catch (\Exception $e) {

			return response()->json([
				'status' => false,
				'message' => $data
			], 403);
		}
	}
	
	
	public function show(int $id): JsonResponse
	{
		//$todo = $this->MUom->findOrFail($id);
		//$todo = MStore::find($id);
		$todo =  MStore::selectRaw('
		"m_store"."storeCode",
		"m_store"."bpCustCode",
		"m_store"."address",
		"m_store"."phone",
		"m_store"."storeName",
		"m_store"."level",
		
		"m_store"."whsCode",
		"m_store"."priceCode",
		"m_store"."discCode",
		
		

		"m_price"."priceName" as "harga_info",
         "m_discount"."discName" as "diskon_info",
         "m_warehouse"."whsName" as "gudang_info"
		
		
		
')
            ->where('m_store.id', '=', $id)
			 ->join('m_price', 'm_price.priceCode', '=', 'm_store.priceCode')
            ->join('m_discount', 'm_discount.discCode', '=', 'm_store.discCode')
            ->join('m_warehouse', 'm_warehouse.whsCode', '=', 'm_store.whsCode')
			->get();
		if ($todo) {
			//return response()->json(['data' => $todo], 200);
			return response()->json(['data' => $todo[0]], 200);
		} else {
			return response()->json([
				'status' => false,
				'message' => $this->judul_halaman_notif . ' data not found.',
			], 404);
		}
	}
	
	public function update(Request $request, int $id): JsonResponse
	{
		//validate incoming request 
		//echo "<pre>";echo print_r($request);die();

		$user_id = $request->userid;
		
		$data = $this->validate($request, [
		
			'data.storeCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.bpCustCode' => 'regex:/^[a-zA-Z0-9\s]+$/',
			'data.address' => 'regex:/^[a-zA-Z0-9\s]+$/',
			'data.phone' => 'regex:/^[a-zA-Z0-9\s]+$/',
			'data.storeName' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.priceCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.discCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.whsCode' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
			'data.level' => '',
			
		]);

		try {
			$todo = MStore::findOrFail($id);
			$todo->fill($data['data']);
			$todo->save();

			MStore::where('id', $id)->update(['updated_by' => $user_id, 'updated_at' => date('Y-m-d H:i:s')]);

			//return successful response
			return response()->json([
				'status' => true,
				'message' => $this->judul_halaman_notif . ' updated successfully.',
				'data' => $todo
			], 201);
		} catch (\Exception $e) {
	
			//return error message
			return response()->json([
				'status' => false,
				'message' => $this->judul_halaman_notif . ' failed update.',
			], 409);
		}
	}
	
	
	public function getCbostoreCode(): JsonResponse
	{
		$type = (new MStore())->getCbostoreCode();
		return response()->json(
			['data' => $type],
			200
		);
	}
	
	
	public function alldata(Request $request): JsonResponse
    {

		
		///////////////////////////////////////////////////////////////
		
	try {
		
		if (MKeyApi::where('APIkey', $request->APIkey)
		->where('isActive', 't')	
		->count() > 0) {
			
     $todos = MStore::offset($request->offset)->limit($request->limit)
		->orderBy("id")->get();
		
		$count = $todos->count();
		
        return response()->json(
            [
			'status' => true,
			'count' => $count,
			'data' => $todos],
            200
        );
		
		}else{
			return response()->json([
                'status' => false,
				'message' => 'API key is invalid',
            ], 401);
		
		}
		
		
		} catch (\Exception $e) {
			  return response()->json([
                'status' => false,
				'message' => 'error get data',
            ], 403);
		}
		///////////////////////////////////////////////////////////////
    }
	
}
